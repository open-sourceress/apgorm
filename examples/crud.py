import asyncio
import asyncpg
import random
import sys

import apgorm
from apgorm import pg_types


class Contact(apgorm.Table, table_name='contacts'):
    id: pg_types.serial = apgorm.Column()
    name: pg_types.varchar = apgorm.Column()
    pronouns: pg_types.varchar = apgorm.Column()
    email: pg_types.nullable[pg_types.varchar] = apgorm.Column()
    phone: pg_types.nullable[pg_types.varchar] = apgorm.Column()

    constraints = (apgorm.PrimaryKey(id), apgorm.Unique(email), apgorm.Unique(phone))


async def insert():
    alexis = Contact(name='Alexis', pronouns='they/them', email='alexis@example.com', phone='123-555-4567')
    await alexis.insert()  # INSERT INTO contacts (name, pronouns, email, phone_number) VALUES ...
    # Inserting also updates the record with the actually inserted values, meaning it has an ID now
    print(f'{alexis.name}\'s ID is {alexis.id}')

    claudine = Contact(name='Claudine', pronouns='she/her', email='claudine@example.com')
    await claudine.insert()
    print(f'{claudine.name}\'s ID is {claudine.id} and her phone number is {claudine.phone}')

    # Type and constraint validation are delegated to Postgres, and therefore don't happen until the record is
    # inserted. No attempt is made to map Postgres types to Python types beyond type hints in column declarations.
    bad_contact = Contact(name=4)  # Name is wrong type, and pronouns (non-nullable) is absent
    try:
        await bad_contact.insert()
    except (asyncpg.DataError, asyncpg.NotNullViolationError) as e:
        print(f'Error inserting invalid contact: {e}')
    else:
        raise RuntimeError('Inserting bad contact did not error')

    # Because type- and required-value checking are delayed, records can be assembled piecewise and finally inserted
    # when completely initialized
    leo = Contact()
    leo.name = 'Leo'
    leo.pronouns = 'he/him'
    if random.randrange(2):
        leo.email = 'leo@example.com'
    if random.randrange(2):
        leo.phone = '987-555-6543'
    await leo.insert()
    print(f'{leo.name}\'s ID is {leo.id}, his email is {leo.email}, and his phone number is {leo.phone}')

    return (alexis, claudine, leo)


async def update(alexis: Contact, claudine: Contact, leo: Contact):
    old_number, alexis.phone = alexis.phone, "456-555-1289"
    await alexis.update()
    print(f'Alexis\' phone number updated from {old_number} to {alexis.phone}')

    # update() can be called even if the record hasn't been changed. In that case, it is a no-op.
    await claudine.update()

    old_number = leo.phone
    if leo.phone is None:
        leo.phone = '432-555-1928'
    else:
        # Arbitrary process representing updating the value
        p1, p2, p3 = leo.phone.split("-")
        new = (p1 + p3)[::-1]
        p1, p3 = new[:3], new[3:]
        leo.phone = f'{p1}-{p2}-{p3}'

    await leo.update()
    print(f'Leo\'s phone number updated from {old_number} to {leo.phone}')


async def main(pg_dsn: str) -> None:
    async with apgorm.connect(pg_dsn):
        await Contact.create()

        contacts = await insert()
        await update(*contacts)

        for contact in contacts:
            await contact.delete()
            print(f'Deleted {contact.name} from contacts')

        await Contact.drop()


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit('Pass a DSN (postgres://user:pass@host:port/db?option=value) to connect to')
    dsn = sys.argv[1]
    asyncio.run(main(dsn))
