import asyncio
import sys

import asyncpg

import apgorm


async def main_use_pool(loop: asyncio.AbstractEventLoop, pg_dsn: str) -> None:
    pool = await asyncpg.create_pool(pg_dsn, loop=loop)
    async with apgorm.use_pool(pool):
        print('Result from use_pool:', await child())
    await pool.close()


async def main_create_pool(loop: asyncio.AbstractEventLoop, pg_dsn: str) -> None:
    async with apgorm.create_pool(pg_dsn, loop=loop) as pool:
        # demonstrate how acquire() works with a connection pool
        async with apgorm.acquire() as acquired_conn:
            assert pool is not acquired_conn

        # acquired connection is released after acquire() exits...
        try:
            acquired_conn.is_closed()
        except asyncpg.InterfaceError:
            pass  # "cannot call Connection.is_closed(): connection has been released back to the pool
        else:
            raise AssertionError('connection was not released')

        # ...but pool is not closed
        print('Result from create_pool:', await child())


async def child() -> str:
    async with apgorm.acquire() as conn:
        ret = await conn.fetchval("SELECT 'hello, apgorm!';")
    return ret


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit('Pass a DSN (postgres://user:pass@host:port/db?option=value) to connect to')
    dsn = sys.argv[1]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(main_use_pool(loop, dsn), main_create_pool(loop, dsn)))
