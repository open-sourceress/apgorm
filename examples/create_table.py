import asyncio
import sys

import apgorm
from apgorm import pg_types


class ExampleTable(apgorm.Table, table_name='example_table'):
    # In simplest case, both name and type can be inferred
    id: pg_types.serial = apgorm.Column()
    # Even some types that aren't Python identifiers are representable
    name: pg_types.varchar(10) = apgorm.Column()
    # Nullability can be inferred from annotations
    extra: pg_types.nullable[pg_types.int8] = apgorm.Column()
    # Postgres types with multi-word names use underscores instead
    inserted_at: pg_types.timestamp_with_time_zone = apgorm.Column()


async def main(loop: asyncio.AbstractEventLoop, pg_dsn: str) -> None:
    async with apgorm.connect(pg_dsn, loop=loop):
        # CREATE TABLE IF NOT EXISTS example_table (id serial NOT NULL, name varchar(10) NOT NULL, extra int8 NULL,
        # inserted_at timestamp with time zone NOT NULL);
        await ExampleTable.create(exist_ok=True)
        # DROP TABLE example_table;
        await ExampleTable.drop()


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit('Pass a DSN (postgres://user:pass@host:port/db?option=value) to connect to')
    dsn = sys.argv[1]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop, dsn))
