import asyncio
import sys

import asyncpg

import apgorm


async def main_use_connection(loop: asyncio.AbstractEventLoop, pg_dsn: str) -> None:
    conn: asyncpg.Connection = await asyncpg.connect(pg_dsn, loop=loop)
    async with apgorm.use_connection(conn):
        print('Result from use_connection:', await conn.fetchval("SELECT 'hello, apgorm!';"))
    await conn.close()


async def main_connect(loop: asyncio.AbstractEventLoop, pg_dsn: str) -> None:
    async with apgorm.connect(pg_dsn, loop=loop) as conn:
        # demonstrate how acquire() works with a single connection
        async with apgorm.acquire() as acquired_conn:
            assert conn is acquired_conn

        # connection is not closed after acquire() exits
        print('Result from connect:', await conn.fetchval("SELECT 'hello, apgorm!';"))


if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit('Pass a DSN (postgres://user:pass@host:port/db?option=value) to connect to')
    dsn = sys.argv[1]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(main_use_connection(loop, dsn), main_connect(loop, dsn)))
