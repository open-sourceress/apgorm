import abc
import contextvars
import types
import typing

import asyncpg

C = typing.TypeVar('C')
E = typing.TypeVar('E', bound=BaseException)

# (connection/pool, is_a_pool) - is_a_pool is determined by whether the caller called connect/use_connection or
# create_pool/use_pool, rather than inheritance, so that pools can be used as single connections and pool-like objects
# can be used as pools
_connection = contextvars.ContextVar('_connection', default=(None, False))


class ConnectionError(Exception):
    """Base class for exceptions caused by issues connecting to Postgres."""


class NotConnectedError(ConnectionError):
    """Exception raised when `acquire` is used without an outer connection manager to acquire a connection from."""


class ReentranceError(ConnectionError):
    """Exception raised when a single connection manager is used while it is already in use."""


class ConnectionManager(abc.ABC, typing.Generic[C]):
    """
    Connection managers provide connections (and/or connection pools) to be used for ORM operations.

    Connection managers can be used two ways:
    1. Use the connection manager in an async-with block
    1. Call `ConnectionManager.use` before it's needed, and `ConnectionManager.reset` after

    In the async-with block/after calling use and before calling reset, the connection/pool provided by this connection
    manager will be used for ORM operations.

    Connection managers' blocks can be nested inside one another. The inner connection manager will be used for its
    inner block like normal, and the outer block will be in use again after the inner one exits. For example:::

        # no connection is in use here
        async with apgorm.create_pool(...):
            # pool from create_pool is in use here
            async with apgorm.acquire():
                # connection from acquire is in use here
                ...
            # pool from create_pool is in use here
        # no connection is in use here

    Connection managers can be used multiple times, but not reentrantly.::

        conn_mgr = apgorm.connect(...)
        async with conn_mgr:
            # async with conn_mgr:  # this would raise a ReentranceError
            ...
        # after conn_mgr has exited, it can be used again
        async with conn_mgr:
            ...
    """
    __slots__ = ('_conn', '_token', '_is_pool')
    _conn: typing.Optional[C]
    _token: typing.Optional[contextvars.Token]
    _is_pool: bool

    def __init__(self, is_pool: bool):
        self._conn = None
        self._token = None
        self._is_pool = is_pool

    async def use(self) -> C:
        """Use this connection manager's connection for ORM operations."""
        if self._token is not None:
            raise ReentranceError(f'{type(self).__qualname__} block used twice, or used without being reset')
        self._conn = conn = await self._make_connection()
        self._token = _connection.set((conn, self._is_pool))
        return conn

    async def reset(self) -> None:
        """Stop using this connection manager's connection for ORM operations."""
        if self._token is None:
            raise ReentranceError(f'{type(self).__qualname__} block reset without being used, or reset twice')
        _connection.reset(self._token)
        self._token = None
        self._conn = await self._destroy_connection()

    @property
    def in_use(self) -> bool:
        """Whether this connection manager is currently in use."""
        return self._token is not None

    async def __aenter__(self) -> C:
        return await self.use()

    async def __aexit__(self, exc_type: typing.Type[E], exc_val: E, exc_tb: types.TracebackType) -> bool:
        await self.reset()
        return False

    @abc.abstractmethod
    async def _make_connection(self) -> C:
        """Make a connection before this block is used."""

    @abc.abstractmethod
    async def _destroy_connection(self) -> None:
        """Tear down the connection after this block is used and reset."""


class UseConnection(ConnectionManager[C], typing.Generic[C]):
    """
    A connection manager that uses an existing connection.
    See `use_connection` for more information.
    """
    __slots__ = ()

    def __init__(self, conn: C) -> None:
        super().__init__(False)
        self._conn = conn

    async def _make_connection(self) -> C:
        return typing.cast(C, self._conn)  # connection is never None

    async def _destroy_connection(self) -> None:
        pass  # The caller created the connection, so they're responsible for closing it


def use_connection(conn: C) -> UseConnection[C]:
    """
    Use an existing connection for ORM operations.

    The caller is responsible for closing the connection after this block is finished.

    :param conn: connection to use
    :return: a `ConnectionManager` that uses the given connection
    """
    return UseConnection(conn)


class Connect(ConnectionManager[C], typing.Generic[C]):
    """
    A connection manager that creates a new connection when used.
    See `connect` for more information.
    """
    __slots__ = ('_args', '_kwargs')

    def __init__(self, *args, connection_class: typing.Type[C] = asyncpg.Connection, **kwargs) -> None:
        super().__init__(False)
        self._args = args
        self._kwargs = kwargs
        self._kwargs['connection_class'] = connection_class

    async def _make_connection(self) -> C:
        return await asyncpg.connect(*self._args, **self._kwargs)

    async def _destroy_connection(self) -> None:
        await typing.cast(C, self._conn).close()  # self._conn is only None after this method is called
        self._conn = None


def connect(*args, connection_class: typing.Type[C] = asyncpg.Connection, **kwargs) -> Connect[C]:
    """
    Create a connection to use for ORM operations.

    The created connection is closed internally and should not be closed by the caller.

    :param args: arguments to pass to `asyncpg.connect` when creating a new connection
    :param connection_class: class of the connection created (passed to `asyncpg.connect`)
    :param kwargs: keyword arguments to pass to `asyncpg.connect` when creating a new connection
    :return: a `ConnectionManager` that creates a connection when used
    """
    return Connect(*args, connection_class=connection_class, **kwargs)


class UsePool(ConnectionManager[asyncpg.pool.Pool]):
    """
    A connection manager that uses an existing connection pool.
    See `use_pool` for more information.
    """
    __slots__ = ()

    def __init__(self, pool: asyncpg.pool.Pool) -> None:
        super().__init__(True)
        self._conn = pool

    async def _make_connection(self) -> asyncpg.pool.Pool:
        return self._conn

    async def _destroy_connection(self) -> None:
        pass  # The caller created the pool, so they're responsible for closing it


def use_pool(pool: asyncpg.pool.Pool) -> UsePool:
    """
    Use an existing connection pool for ORM operations.

    Inside this block, each operation will acquire a new connection and release it afterward. To reuse
    a single acquired connection for multiple operations, use `acquire` inside this block.

    The caller is responsible for closing the pool after this block is finished.

    :param pool: connection pool to use
    :return: a `ConnectionManager` that uses the given connection pool
    """
    return UsePool(pool)


class CreatePool(ConnectionManager[asyncpg.pool.Pool]):
    """
    A connection manager that creates a new connection pool when used.
    See `create_pool` for more information.
    """
    __slots__ = ('_args', '_kwargs')

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(True)
        self._args = args
        self._kwargs = kwargs

    async def _make_connection(self) -> asyncpg.pool.Pool:
        self._conn = pool = await asyncpg.create_pool(*self._args, **self._kwargs)
        await pool._async__init__()
        return pool

    async def _destroy_connection(self) -> None:
        await typing.cast(asyncpg.pool.Pool, self._conn).close()  # self._conn is only None after this method is called


def create_pool(*args, **kwargs) -> CreatePool:
    """
    Create a connection pool to use for ORM operations.

    Inside this block, each operation will acquire a new connection and release it afterward. To reuse
    a single acquired connection for multiple operations, use `acquire` inside this block.

    The created pool is closed internally and should not be closed by the caller.

    :param args: arguments to pass to `asyncpg.create_pool` when creating a new connection
    :param kwargs: keyword arguments to pass to `asyncpg.create_pool` when creating a new connection
    :return: a `ConnectionManager` that creates a connection pool when used
    """
    return CreatePool(*args, **kwargs)


class Acquire(ConnectionManager[asyncpg.Connection]):
    """
    A connection manager that acquires connections from a previously-used pool when used.
    See `acquire` for more information.
    """
    __slots__ = ('_timeout', '_pool')

    def __init__(self, timeout: float = None) -> None:
        super().__init__(True)
        self._timeout = timeout
        self._pool: typing.Optional[asyncpg.pool.Pool] = None

    async def _make_connection(self) -> asyncpg.Connection:
        pool, is_pool = _connection.get((None, None))
        if is_pool is None:  # hit default - no connection has been registered at all
            raise NotConnectedError('No connection has been made')
        if is_pool:
            self._pool = pool
            return await pool.acquire(timeout=self._timeout)
        else:
            self._pool = None
            return pool

    async def _destroy_connection(self) -> None:
        if self._pool:
            await self._pool.release(self._conn)
        self._conn = None
        self._pool = None


def acquire(*, timeout: float = None) -> Acquire:
    """
    Acquire a connection, either a used connection or a member of a used pool, to use for ORM operations.

    If the last connection manager in use was created with `use_pool` or `connect_pool`, a connection is acquired from
    that pool when this block is used and released at the end of this block. If not, the connection used by the last
    connection manager is returned.

    :param timeout: timeout, in seconds, for acquiring a connection from a pool to wait
    :return: a `ConnectionManager` that acquires a connection from a previously-used connection manager when used
    """
    return Acquire(timeout)
