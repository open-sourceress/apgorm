import collections
import inspect
import sys
import typing

import asyncpg

from . import acquire, constraints, pg_types, pg_types_internal as types_impl

Bases = typing.Tuple[type, ...]
T = typing.TypeVar('T', bound='Table')

if typing.TYPE_CHECKING:
    SqlType = typing.Union[typing.Type, str]  # if type-checking, accept pg_types.* (classes) or literals (str)
else:
    SqlType = str


class TableDefinitionError(Exception):
    """Exception raised when an invalid table is constructed."""


class NoPrimaryKeyError(Exception):
    """Exception raised when an operation that requires a primary key is called on a table without one."""

    def __init__(self, table: 'typing.Type[Table]', method: str, *args) -> None:
        super().__init__(f'{method}() cannot be called on table {table} with no primary key', *args)


class Column:
    """Column in a table definition."""
    __slots__ = ('_name', '_type', '_nullable', '_attr_name', '_slot_name')
    _name: typing.Optional[str]
    _type: typing.Optional[SqlType]
    _nullable: typing.Optional[bool]

    _attr_name: typing.Optional[str]
    _slot_name: typing.Optional[str]

    def __init__(self, name: str = None, type: SqlType = None, nullable: bool = None):
        if type is not None and not isinstance(type, str):
            raise TypeError('column type must resolve to string, was {!r}', type)
        self._name = name
        self._type = type
        self._nullable = nullable
        self._attr_name = None
        self._slot_name = None

    @property
    def name(self) -> str:
        """Name of the column."""
        if self._name is None:
            raise RuntimeError('attempted to get column name before it is initialized')
        return self._name

    @property
    def type(self) -> str:
        """Postgres type of the column, as a string."""
        if self._type is None:
            raise RuntimeError('attempted to get column type before it is initialized')
        return self._type

    @property
    def nullable(self) -> bool:
        """Returns true if this column is allowed to contain NULL."""
        if self._nullable is None:
            raise RuntimeError('attempted to get column nullability before it is initialized')
        return self._nullable

    def __get__(self, instance: typing.Optional[T], owner: typing.Type[T]) -> typing.Any:
        # TODO: annotate column type and use as return type here
        if instance is None:
            return self

        val = getattr(instance, self._slot_name)
        if val is TableMeta._default:
            raise AttributeError(f'Column {owner.__qualname__}.{self._attr_name} accessed before it was set')
        else:
            return val

    def __set__(self, instance: T, value: typing.Any) -> None:
        # TODO: constrain value to the column's type
        prev = getattr(instance, self._slot_name)
        if prev is not TableMeta._default and self._attr_name not in instance._dirty_values:
            instance._dirty_values[self._attr_name] = prev
        setattr(instance, self._slot_name, value)

    def __delete__(self, instance: T) -> None:
        prev = getattr(instance, self._slot_name)
        if prev is not TableMeta._default and self._attr_name not in instance._dirty_values:
            instance._dirty_values[self._attr_name] = prev
        setattr(instance, self._slot_name, TableMeta._default)


class TableMeta(type):
    # for type-checking
    table_name: str
    columns: typing.Tuple[Column, ...]
    constraints: 'typing.Tuple[constraints.Constraint, ...]'
    _default = object()  # marker for value being/set to the default

    @classmethod
    def __prepare__(mcs, __name: str, __bases: Bases, **kwargs) -> typing.Mapping[str, typing.Any]:
        return collections.OrderedDict(super().__prepare__(__name, __bases, **kwargs))

    def __new__(mcs, cls_name: str, bases: Bases, members: typing.MutableMapping[str, typing.Any],
                **kwargs) -> 'TableMeta':
        cols = collections.OrderedDict((name, value) for name, value in members.items() if isinstance(value, Column))
        for name, column in cols.items():
            column._attr_name = name
            column._slot_name = f'_{name}'
        members['table_name'] = kwargs.pop('table_name', None)  # Table base class has no table_name
        members['columns'] = tuple(cols.values())
        members['constraints'] = members.get('constraints', ())
        members['__slots__'] = members.get('__slots__', ()) + tuple(col._slot_name for col in cols.values())

        # Cast is safe because created class is ensured to be an instance of its metaclass
        cls = typing.cast(TableMeta, super().__new__(mcs, cls_name, bases, dict(members)))

        # localns (first one checked) is module's globals, globalns is pg_types's globals
        # If the caller has column_name: pg_types.serial, get_type_hints checks pg_types' globals and finds 'serial',
        # evals that and gets a global lookup into pg_types' globals again, and returns 'serial'
        # Passing module globals as localns allows user-specified types (e.g. types provided by extensions)
        annotations = typing.get_type_hints(cls, vars(pg_types), vars(sys.modules[cls.__module__]))
        ctor_params = [inspect.Parameter('__record', inspect._ParameterKind.POSITIONAL_ONLY, default=None,
                                         annotation=asyncpg.Record)]
        for name, column in cols.items():
            if column._name is None:
                column._name = name
            if name in annotations:
                deduced_type, deduced_nullability = mcs._find_type(name, column, annotations[name])
                if column._type is None:
                    column._type = deduced_type
                if column._nullable is None and deduced_nullability is not None:
                    column._nullable = deduced_nullability
            elif column._type is None:
                raise TableDefinitionError(f'Column {name!r} has no type and no annotation')
            column._type = column._type.replace('_', ' ')

            if column._nullable is None:
                column._nullable = False

            ann = typing.ForwardRef(column.type.replace(' ', '_'))
            if column.nullable:
                ann = typing.Optional[ann]
            ctor_params.append(inspect.Parameter(column._attr_name, inspect._ParameterKind.KEYWORD_ONLY, annotation=ann,
                                                 default=mcs._default))

        cls._signature = inspect.Signature(ctor_params)

        return cls

    @classmethod
    def _find_type(mcs, attr_name: str, column: Column, ann) -> typing.Tuple[str, typing.Optional[bool]]:
        """Deduce a Postgres type and nullability from an annotation."""
        nullable = None
        if isinstance(ann, str):
            type_name = ann
        elif isinstance(ann, typing.ForwardRef):
            type_name = ann.__forward_arg__
        elif isinstance(ann, types_impl.ParameterizedType):  # e.g. `varchar` but not `varchar(16)`
            type_name = ann._base
        elif isinstance(ann, types_impl.Nullable):
            type_name, _ = mcs._find_type(attr_name, column, ann._inner)
            nullable = True
        elif column._type is not None:
            type_name = column._type
        else:
            raise TableDefinitionError(f'Column {attr_name!r} has no type and invalid annotation {ann!r}')
        return (type_name, nullable)

    async def create(cls, *, exist_ok: bool = False, timeout: float = None) -> None:
        """
        Create the table represented by this class.
        :param exist_ok: if truthy, a ``CREATE TABLE IF NOT EXISTS`` statement will be issued
        :param timeout: timeout on executing the ``CREATE TABLE`` statement, in seconds
        """
        builder = ['CREATE TABLE']
        if exist_ok:
            builder.append('IF NOT EXISTS')
        builder.append(cls.table_name)
        builder.append('(')

        for column in cls.columns:
            builder.append(column.name)
            builder.append(column.type)
            if not column.nullable:
                builder.append('NOT NULL')
            builder.append(',')

        for constraint in cls.constraints:
            constraint.to_sql(builder)
            builder.append(',')

        builder[-1] = ');'  # Replace trailing comma
        async with acquire() as conn:
            await conn.execute(' '.join(builder), timeout=timeout)

    async def drop(cls, *, missing_ok: bool = False, timeout: float = None):
        """
        Drop the table represented by this class.
        :param missing_ok: if truey, a ``DROP TABLE IF EXISTS`` statement will be issued
        :param timeout: timeout on executing the ``DROP TABLE`` statement, in seconds
        """
        builder = ['DROP TABLE']
        if missing_ok:
            builder.append('IF EXISTS')
        builder.append(cls.table_name)
        builder.append(';')
        async with acquire() as conn:
            await conn.execute(' '.join(builder), timeout=timeout)

    @property
    def _primary_key(cls) -> 'typing.Optional[constraints.PrimaryKey]':
        """Get the primary constraint on this table, or ``None`` if one does not exist."""
        for constraint in cls.constraints:
            if isinstance(constraint, constraints.PrimaryKey):
                return constraint
        return None


class Table(metaclass=TableMeta):
    """
    Base class for table definitions.

    Table classes should subclass this class and include columns as members.
    The table name must be passed as a keyword argument after the base class list.::

        class ExampleTable(apgorm.Table, table_name='example_table'):
            name = apgorm.Column(type='text')
            ...
    """
    __slots__ = ('_dirty_values',)
    _dirty_values: typing.Dict[str, typing.Any]  # values as they were in PG - used to identify record when PK changes

    # attributes set by metaclass, annotated here for type-checking that can't determine that
    table_name: str
    columns: typing.Tuple[Column, ...]

    def __init__(self, *args, **kwargs) -> None:
        """
        Create a record from key-value pairs, and/or from a record retrieved from Postgres.

        While this constructor is declared to take any number of arguments, at runtime the signature is checked against
        the table's column list. There must be at most one positional argument, which is an `asyncpg.Record` or
        ``None``. Keyword arguments must be a name of a column in this table.

        Note that types are not checked and default values are not populated until this record is inserted.
        """
        bound = self._signature.bind(*args, **kwargs)
        if bound.args:
            record, = bound.args
        else:
            record = None
        self._dirty_values = {}
        self._update(record, bound.kwargs)

    def _update(self, record: typing.Optional[asyncpg.Record], kwargs: typing.Mapping[str, typing.Any] = None) -> None:
        """
        Populate ``self`` with values from the keyword arguments and/or the record.

        Keyword arguments take precedence over record items.
        """
        for column in type(self).columns:
            val = TableMeta._default
            if kwargs is not None:
                val = kwargs.get(column._attr_name, TableMeta._default)
            if val is TableMeta._default and record is not None:
                val = record.get(column.name, TableMeta._default)
            if val is TableMeta._default:
                val = getattr(self, column._slot_name, TableMeta._default)
            setattr(self, column._slot_name, val)
        self._dirty_values.clear()

    async def insert(self, *, timeout: float = None) -> None:
        """
        Insert this record into its table.

        No validity checks are done aside from those performed by Postgres. If ``self`` contains values of the wrong
        type for the column, doesn't contain a value for a column that has no default value, violate's the table's
        constraints, etc, the exception raised by asyncpg is bubbled up to the caller. In this case, the contents of
        this object after the exception has been raised are unspecified and callers should re-fetch the record from
        Postgres if needed.
        """
        builder = ['INSERT INTO', self.table_name, '(']

        # isolate columns that are set to something/not the default
        values = [(column, getattr(self, column._slot_name)) for column in self.columns]
        to_insert = [(column, value) for column, value in values if value is not TableMeta._default]
        for column, value in to_insert:
            builder.append(column.name)
            builder.append(',')

        if to_insert:
            builder[-1] = ')'
            builder.append('VALUES')
            builder.append('(')
            for i, _ in enumerate(to_insert, 1):
                builder.append(f'${i}')
                builder.append(',')
            builder[-1] = ')'
        else:
            builder[-1] = 'DEFAULT VALUES'
        builder.append('RETURNING *;')
        async with acquire() as conn:
            record = await conn.fetchrow(' '.join(builder), *[value for _, value in to_insert], timeout=timeout)
        self._update(record)

    async def update(self, *, timeout: float = None) -> None:
        """
        Update this record in Postgres with the changes applied to ``self``.

        This method assumes ``self`` has been changed by setting attributes normally. When this method is called, an
        ``UPDATE`` statement is executed that applies those changes to the record in Postgres.

        Warning: if ``self`` contains invalid values, i.e. values of the wrong type for a column or values that a
        violate a constraint, the exception raised by asyncpg is bubbled up to the caller. In this case, the contents of
        this object after the exception has been raised are unspecified and callers should re-fetch the record from
        Postgres if needed.

        In order to uniquely select the appropriate record, this table must have a primary key constraint. If it doesn't
        have a primary key, a `NoPrimaryKeyError` is raised.
        """

        pk = type(self)._primary_key
        if pk is None:
            raise NoPrimaryKeyError(type(self), 'update')

        if not self._dirty_values:  # No changes since last time we synced with the DB
            return

        builder = ['UPDATE', self.table_name, 'SET']
        args = []
        for attr_name in self._dirty_values:
            column = getattr(type(self), attr_name)
            builder.append(column.name)
            builder.append(f'= ${len(args) + 1}')
            builder.append(',')
            args.append(getattr(self, attr_name))

        builder[-1] = 'WHERE'
        for column in pk._columns:
            builder.append(column.name)
            builder.append(f'= ${len(args) + 1}')
            builder.append('AND')
            args.append(self._dirty_values[column._attr_name]  # ternary instead of .get() to lazily evaluate getattr()
                        if column._attr_name in self._dirty_values
                        else getattr(self, column._attr_name))  # Use column's __get__ to raise if the PK isn't set

        builder[-1] = 'RETURNING *;'
        async with acquire() as conn:
            record = await conn.fetchrow(' '.join(builder), *args, timeout=timeout)

    async def delete(self, *, timeout: float = None) -> None:
        """
        Delete this record from Postgres.

        ``self`` is populated with the values of the record at the time of deletion.

        In order to uniquely select the appropriate record, this table must have a primary key constraint. If it doesn't
        have a primary key, a `NoPrimaryKeyError` is raised.
        """

        pk = type(self)._primary_key
        if pk is None:
            raise NoPrimaryKeyError(type(self), 'delete')

        builder = ['DELETE FROM', self.table_name, 'WHERE']
        args = []
        for column in pk._columns:
            builder.append(column.name)
            builder.append(f'= ${len(args) + 1}')
            builder.append('AND')
            args.append(self._dirty_values[column._attr_name]  # ternary instead of .get() to lazily evaluate getattr()
                        if column._attr_name in self._dirty_values
                        else getattr(self, column._attr_name))  # Use column's __get__ to raise if the PK isn't set

        builder[-1] = 'RETURNING *;'
        async with acquire() as conn:
            record = await conn.fetchrow(' '.join(builder), *args, timeout=timeout)
        self._update(record)
