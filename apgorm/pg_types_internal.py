"""
Internal helpers for the `pg_types` module.

These classes imitate the corresponding type hints from the ``typing`` module, but produce forms that are easier for
apgorm to interpret as a Postgres types at runtime.
"""


class Nullable:
    """A nullable wrapper for a Postgres type. Akin to ``typing.Optional[T]``."""
    __slots__ = ('_inner',)

    def __init__(self, inner_type: str) -> None:
        self._inner = inner_type

    def __repr__(self) -> str:
        return f'nullable[{self._inner}]'

    def __call__(self, *args, **kwargs):  # shim to convince `typing.get_type_hints` that this is a class
        raise TypeError(f'attempted to call {self}')


class NullableFactory:
    """A factory for creating nullable types, using indexing syntax. Akin to ``typing.Optional`` without arguments."""
    __slots__ = ()

    def __getitem__(self, inner_type: str) -> Nullable:
        return Nullable(inner_type)


nullable = NullableFactory()


class ParameterizedType:
    """
    A mapping to a Postgres type with optional parameters.

    Using this class, it is possible to represent types with an optional length parameter, e.g. `varchar`:::
        from apgorm import Column, Table, pg_types

        # `pg_types.varchar` is a `ParameterizedType` with base name `'varchar'`

        class Example(Table, table_name='example'):
            limited_length: pg_types.varchar(32) = Column()  # Postgres type: `varchar(32)`
            unlimited_length: pg_types.varchar = Column()  # Postgres type: `varchar`
    """
    __slots__ = ('_base',)

    def __init__(self, base_name: str) -> None:
        self._base = base_name

    def __call__(self, len: int) -> str:
        return f'{self._base}({len})'
