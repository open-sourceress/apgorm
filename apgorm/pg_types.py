"""
Postgres to Python type mappings.

The intended purpose is as annotations on columns in table definitions for type checking. For example, these two
definitions are equivalent, but the second will provide typing information for type checkers:::

    import apgorm
    from apgorm import pg_types

    class ExampleTable(apgorm.Table, table_name='example_table'):
        id = apgorm.Column(type=pg_types.serial)
        ...


    class ExampleTable(apgorm.Table, table_name='example_table'):
        id: pg_types.serial = apgorm.Column()

Type mappings are declared in the order listed in Table 8.1 from the Postgres documentation, shown at
<https://www.postgresql.org/docs/current/datatype.html#DATATYPE-TABLE>. Mappings are taken from asyncpg's documentation
on type conversions, shown at <https://magicstack.github.io/asyncpg/current/usage.html#type-conversion>.
"""
import datetime as _datetime
import decimal as _decimal
import ipaddress as _ipaddress
import typing as _typing
import uuid as _uuid

import asyncpg as _asyncpg

from . import pg_types_internal as _impl

# Each alias must be declared in its own statement to count as a type alias for type checking. Types that Postgres
# treats as aliases for each other are grouped together, with the unaliased type listed first.

bigint = int
int8 = bigint

bigserial = int
serial8 = bigserial

bit = _asyncpg.BitString

bit_varying = _asyncpg.BitString
varbit = bit_varying

boolean = bool
bool = boolean  # intentionally shadow builtin so that it's replaced with the Postgres type name

box = _asyncpg.Box

bytea = bytes

name = str

cidr = _typing.Union[_ipaddress.IPv4Address, _ipaddress.IPv6Address]

circle = _asyncpg.Circle

date = _datetime.date

double_precision = float
float8 = double_precision

inet = _typing.Union[_ipaddress.IPv4Network, _ipaddress.IPv6Network, _ipaddress.IPv4Address, _ipaddress.IPv6Address]

integer = int
int4 = integer
int = int4  # intentionally shadow builtin so that it's replaced with the Postgres type name

interval = _datetime.timedelta

json = str

jsonb = str

line = _asyncpg.Line

lseg = _asyncpg.LineSegment

macaddr = str

macaddr8 = macaddr

money = str

numeric = _decimal.Decimal
decimal = numeric

path = _asyncpg.Path

point = _asyncpg.Point

polygon = _asyncpg.Polygon

real = float
float4 = real

smallint = int
int2 = smallint

smallserial = int
serial2 = smallserial

serial = int
serial4 = serial

text = str

time = _datetime.time  # tzinfo is None
time_without_time_zone = time

timetz = time  # tzinfo is not None
time_with_time_zone = timetz

timestamp = _datetime.datetime  # tzinfo is None
timestamp_without_time_zone = timestamp

timestamptz = _datetime.datetime  # tzinfo is not None
timestamp_with_time_zone = timestamptz

uuid = _uuid.UUID

void = None

xml = str


def _text_type(len: int) -> _typing.Type[str]:
    return str


character = _text_type
char = character

character_varying = _text_type
varchar = character_varying

nullable = _typing.Optional

if not _typing.TYPE_CHECKING:
    # For type-checking purposes, these globals need to be the Python types asyncpg converts them to
    # At runtime for generating CREATE TABLE statements, these need to be Postgres types as strings
    _pg_types = [name for name in globals() if not name.startswith('_')]
    _type = None
    for _type in _pg_types:
        globals()[_type] = _type
    del _type, _pg_types

    nullable = _impl.nullable
    bit = _impl.ParameterizedType('bit')
    bit_varying = _impl.ParameterizedType('bit varying')
    character = _impl.ParameterizedType('character')
    char = _impl.ParameterizedType('char')
    character_varying = _impl.ParameterizedType('character varying')
    varchar = _impl.ParameterizedType('varchar')
