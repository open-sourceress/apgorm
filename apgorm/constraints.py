import abc
import typing

from . import tables


class Constraint(abc.ABC):
    """
    A constraint applied to a table declaration.

    Note: all constrains declared here are spsecified at the table level. While Postgres-flavor SQL allows column
    constraints, every column constraint can be rewritten as an equivalent table constraint. ``NOT NULL`` constraints
    are handled by the ``Column`` type, since Python incorporates nullability in the type.
    """
    __slots__ = ()

    @abc.abstractmethod
    def to_sql(self, builder: typing.List[str]) -> None:
        """
        Write this constraint to SQL, to be included in a table definition.
        Note that there is no way to use prepared statement parameters. This method is intended to be called while
        building a ``CREATE TABLE`` statement, which disallows query parameters. Therefore, any values must be
        interpolated into the statement builder.
        """


class Unique(Constraint):
    """
    A ``UNIQUE`` constraint on one or more columns.

    This constraint requires that no two records have equal values for every included column. For example, a table with
    two columns ``a`` and ``b`` (both type ``int``),

    - The constraint ``Unique(a)`` would reject inserting ``[(1, 2), (1, 3)]``, because  ``a = 1`` is represented twice.
    - The constraint ``Unique(b)`` would allow inserting ``[(1, 2), (1, 3)]``, because each record has a unique value
        for ``b``.
    - The constraint ``Unique(a, b)`` would allow inserting ``[(1, 2), (1, 3)]``, because the records do not have equal
        values for both ``a`` and ``b``.
    - The constraint ``Unique(a, b)`` would reject inserting ``[(1, 2), (1, 2)]``, because both records have the same
        value for both ``a`` and ``b``.

    Note that ``NULL`` is not equal to itself. Therefore, multiple rows can contain ``NULL`` in a column with a
    ``UNIQUE`` constraint.

    For more information, see the ``UNIQUE`` parameter in the
    [PostgreSQL documentation for the ``CREATE TABLE`` statement][pg_docs].

    [pg_docs]: https://www.postgresql.org/docs/current/sql-createtable.html
    """
    __slots__ = ('_columns',)
    _columns: 'typing.Tuple[tables.Column, ...]'

    def __init__(self, *columns: 'tables.Column') -> None:
        self._columns = columns

    def to_sql(self, builder: typing.List[str]) -> None:
        builder.append('UNIQUE (')
        for column in self._columns:
            builder.append(column.name)
            builder.append(',')
        builder[-1] = ')'


class PrimaryKey(Constraint):
    """
    A ``PRIMARY KEY`` constraint on one or more columns.

    A ``PRIMARY KEY``constraint is a stronger version of a ``UNIQUE`` constraint. In addition to requiring that the
    listed columns be unique, it requires that all columns are non-null. Additionally, this constraint informs
    PostgreSQL that the user intends to look records up using the provided columns as the key, which allows it to
    optimize for said lookups.

    For more information, see the ``PRIMARY KEY`` parameter in the
    [PostgreSQL documentation for the ``CREATE TABLE`` statement][pg_docs].

    [pg_docs]: https://www.postgresql.org/docs/current/sql-createtable.html
    """
    __slots__ = ('_columns',)
    _columns: 'typing.Tuple[tables.Column, ...]'

    def __init__(self, *columns: 'tables.Column') -> None:
        self._columns = columns

    def to_sql(self, builder: typing.List[str]) -> None:
        builder.append('PRIMARY KEY (')
        for column in self._columns:
            builder.append(column.name)
            builder.append(',')
        builder[-1] = ')'


class Named(Constraint):
    """
    Wrapper for a constraint that provides a name for the generated constraint.

    By default, Postgres generates a unique name for each constraint as it is declared. This type allows using a
    specific name for the constraint instead. When an ``INSERT``/``UPDATE``/etc. statement violates a constraint, the
    constraint name is shown in the debug output, so constraint names can be used to provide useful information about
    the issue to debuggers.

    For more information, see the ``CONSTRAINT`` parameter in the
    [PostgreSQL documentation for the ``CREATE TABLE`` statement][pg_docs].

    [pg_docs]: https://www.postgresql.org/docs/current/sql-createtable.html
    """
    __slots__ = ('_name', '_constraint')
    _name: str
    _constraint: Constraint

    def __init__(self, name: str, constraint: Constraint) -> None:
        self._name = name
        self._constraint = constraint

    def to_sql(self, builder: typing.List[str]) -> None:
        builder.append('CONSTRAINT')
        builder.append(self._name)
        self._constraint.to_sql(builder)
