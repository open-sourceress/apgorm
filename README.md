# apgorm
apgorm is a lightweight Postgres mini-ORM built on asyncpg. apgorm turns idiomatic Python into idiomatic SQL for the most common use-cases and gets out of the way beyond that. Unlike typical ORMs, apgorm gives you access to the underlying connection, granting you concise Python when you want it and the full power of PostgreSQL when you need it.

## Examples

See the `examples/` directory for more examples.

```python
import datetime

import apgorm
from apgorm import pg_types

class Friend(apgorm.Table, table_name='friends'):
    id_: pg_types.serial = apgorm.Column('id')
    short_name: pg_types.text = apgorm.Column(type='varchar(32)')
    birthday: pg_types.nullable[pg_types.date] = apgorm.Column()  # Nullable column, type is `Optional[date]`

    constraints = (apgorm.PrimaryKey(id_),)


async def main(postgres_dsn: str):
    async with apgorm.connect(postgres_dsn) as conn:
        # The currently-used connection is tracked internally for each task
        # Because of this, ORM operations don't require passing around an explicit reference to the connection
        await Friend.create()  # CREATE TABLE friends (id serial NOT NULL, ...)

        friend = Friend(short_name='Riley')
        await friend.insert()  # INSERT INTO friends (short_name) VALUES ($1);  param1='Riley'
        print(friend.id_)

        friend.birthday = datetime.date(2000, 1, 23)
        await friend.update()  # UPDATE friends SET birthday = $1 WHERE id = $2;  param1=friend.id_ param2=2000-01-23

        # However, the conn is still available for advanced use-cases not covered by the ORM
        await conn.execute(f"""UPDATE {Friend.table_name} SET short_name = lower(short_name);""")

        await Friend.drop()  # DROP TABLE friends;
    # Connection is automatically closed
```
