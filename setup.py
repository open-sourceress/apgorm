# coding=utf-8
import setuptools

with open('README.md', 'r') as readme:
    long_description = readme.read()

with open('requirements.txt') as reqs:
    requirements = [line.strip() for line in reqs]

setuptools.setup(
    name='apgorm',
    version='0.0.2a0',
    author='open-sourceress',
    author_email='hiyacynth0@gmail.com',
    description='A lightweight Postgres mini-ORM built on asyncpg',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/open-sourceress/apgorm',
    packages=setuptools.find_packages(),
    install_requires=requirements,
    python_requires='>=3.5',
    classifiers=(
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: Implementation :: CPython',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Development Status :: 1 - Planning',
        'Framework :: AsyncIO',
        'Natural Language :: English',
        'Topic :: Database :: Front-Ends'
    )
)
